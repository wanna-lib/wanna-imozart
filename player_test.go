package imozart

import (
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"gitlab.com/wanna-lib/wanna-imozart/player"
	"golang.org/x/crypto/pbkdf2"
	"testing"
	"time"
)

type CreateUser struct {
	Prefix         string `json:"prefix"`
	PlayerUsername string `json:"playerUsername"`
	PlayerPassword string `json:"playerPassword"`
	SecretKey      string `json:"secretKey"`
}

var prefix = "wanabet"

var ctx = Init(Configuration{
	SecretKey: "1e5b6d30-5b21-11ed-8bd4-d55b4ddde80e",
	Host:      "http://10.104.0.2:4000/api/v3",
	Salt:      "salt",
})

func TestGetSignature(t *testing.T) {

	b, err := json.Marshal(CreateUser{
		Prefix:         "test123",
		PlayerUsername: "usertest123",
		PlayerPassword: "123456",
		SecretKey:      "secretKey",
	})
	if err != nil {
		panic(err)
	}

	fmt.Println(string(b))

	salt := []byte("salt")
	a := pbkdf2.Key(b, salt, 300000, 32, sha512.New)
	encodedString := hex.EncodeToString(a)
	assert.Equal(t, encodedString, "9c4208512145078c0045f7701754b6067c20967e780bbdf22ddf8612c5405f49")
}

func TestCreatePlayer(t *testing.T) {
	start := ctx.Start()
	username := "testusder072x"
	password := "123456"

	var response player.ResponseCreatePlayer
	if err := start.PlayerCtx.Create(prefix, player.RequestCreate{
		Username: username,
		Password: password,
	}, &response).Error(); err != nil {
		panic(err)
	}

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}

	imozart_helper.PrintStructJson(response)
}

func TestBalance(t *testing.T) {
	start := ctx.Start()
	username := "bluelomsqjpb6vx5"
	//username := "bluelhqmoci4llc9"

	var response player.ResponseBalance
	if err := start.PlayerCtx.Balance(prefix, username, &response).Error(); err != nil {
		panic(err)
	}

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	imozart_helper.PrintStructJson(response)
}

func TestDeposit(t *testing.T) {
	start := ctx.Start()
	username := "testuser072x"
	amount := 1.00

	var response player.ResponseDeposit
	if err := start.PlayerCtx.Deposit(prefix, username, amount, &response).Error(); err != nil {
		panic(err)
	}

	if !response.Success {
		fmt.Println(response.Code)
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	imozart_helper.PrintStructJson(response)
}

func TestWithdraw(t *testing.T) {
	start := ctx.Start()
	username := "testdfus4mpnplum"
	amount := 1.0

	var response player.ResponseWithdraw
	if err := start.PlayerCtx.Withdraw(prefix, username, amount, &response).Error(); err != nil {
		panic(err)
	}

	fmt.Println(response.Code)
	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	imozart_helper.PrintStructJson(response)
}

func TestHistory(t *testing.T) {
	// ขึ้นแค่ยอดเติมไม่ขึ้นยอดเล่น
	start := ctx.Start()
	username := "abcdec58p72tycvt"
	pagination := player.Pagination{
		Page:    1,
		PerPage: 20,
	}
	filter := player.FilterDate{
		StartDate: "2022-01-01T14:00:00.377Z",
		EndDate:   "2022-11-15T11:00:00.377Z",
	}
	//filter := player.FilterDate{
	//	StartDate: time.Now().Format(time.RFC3339),
	//	EndDate:   time.Now().Format(time.RFC3339),
	//}

	var response player.ResponseHistory
	if err := start.PlayerCtx.History(prefix, player.RequestHistory{
		Username:   username,
		Pagination: pagination,
		Filter:     filter,
	}, &response).Error(); err != nil {
		panic(err)
	}

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	imozart_helper.PrintStructJson(response)
}

func TestTurnover(t *testing.T) {
	start := ctx.Start()
	username := "unicotu2o2zl2rm0"

	//filter := player.FilterDate{
	//	StartDate: time.Now().Add((-time.Hour * 24) * 120).Format(time.RFC3339),
	//	EndDate:   time.Now().Format(time.RFC3339),
	//}

	filter := player.FilterDate{
		StartDate: "2022-07-20T05:21:46.527Z",
		EndDate:   "2023-07-19T05:21:46.527Z",
	}

	var response player.ResponseTurnover
	if err := start.PlayerCtx.Turnover(prefix, username, filter, &response).Error(); err != nil {
		panic(err)
	}

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	imozart_helper.PrintStructJson(response)
}

func TestGameLog(t *testing.T) {
	start := ctx.Start()
	username := "bluelomsqjpb6vx5"

	//filter := player.FilterDate{
	//	StartDate: time.Now().Format(time.RFC3339),
	//	EndDate:   time.Now().Format(time.RFC3339),
	//}

	filter := player.FilterDate{
		StartDate: "2023-01-20T05:38:05.000Z",
		EndDate:   "2023-06-20T10:30:05.000Z",
	}

	fmt.Println(time.Now().Format("2006-01-02T15:04:05.000Z"))

	imozart_helper.PrintStructJson(filter)

	var response player.ResponseGameLog
	if err := start.PlayerCtx.GameLog(prefix, username, filter, &response).Error(); err != nil {
		panic(err)
	}
	//sum := 0.0
	//for _, list := range response.List {
	//	sum += list.Amount
	//}
	//fmt.Println(sum)
	fmt.Println(len(response.List))

	imozart_helper.PrintStructJson(response)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
}

func TestSessionDestroy(t *testing.T) {
	start := ctx.Start()
	username := "bluelf0pbnikt8gw"

	var response player.ResponseSessionDestroy
	if err := start.PlayerCtx.SessionDestroy(prefix, username, &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
}

func TestGetFreeSpin(t *testing.T) {
	start := ctx.Start()
	username := "abcdec58p72tycvt"

	var response player.ResponseGetFreeSpin
	if err := start.PlayerCtx.GetFreeSpin(prefix, username, &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
}

func TestGetOutstanding(t *testing.T) {
	start := ctx.Start()
	username := "abcdec58p72tycvt"

	var response player.ResponseGetOutstanding
	if err := start.PlayerCtx.GetOutstanding(prefix, username, &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
}

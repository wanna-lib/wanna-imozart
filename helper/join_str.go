package imozart_helper

func JoinStr(v ...string) string {
	str := ""
	for _, i := range v {
		str += i
	}
	return str
}

package imozart_helper

var ErrorCodeString = map[int]string{
	0:    "Success",
	3100: "Brand or Agent not found",
	3101: "Cannot use this Signature",
	3200: "User not found",
	3201: "The player’s balance is not enough",
	3900: "This game is already existed in this user",
	4101: "Player Username must use unique characters a-z, 0-9, required, Username length 1 - 30 characters",
	4103: "Username is already in system",
	4104: "Amount is wrong. Required, Min 1, Ex. (1,000.00 or 1000)",
	4105: "Page is wrong. Start, Required, Ex. (1)",
	4106: "perPage is wrong. Limit, Required, Ex. (10)",
	4107: "Start date is wrong. Required, ISOString, Ex. (2020-12-16T14:00:00.377Z)",
	4108: "End date is wrong. Required, ISOString, Ex. (2020-12-16T14:00:00.377Z)",
	4109: "Partner is wrong. Required, Ex. (pt), or not found",
	4110: "Required, Ex. (BAC), or not found",
	4111: "The player’s balance is not enough",
	5100: "A server error has occurred",
	6100: "The providers is not available",
	6200: "Game not in system",
	6300: "Server game error has occurred",
}

package imozart_helper

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetXSignature(t *testing.T) {
	signature, err := GetXSignature(`salt`, map[string]interface{}{
		`prefix`:    "wanabet",
		`secretKey`: "1e5b6d30-5b21-11ed-8bd4-d55b4ddde80e",
	})
	if err != nil {
		panic(err)
	}

	assert.Equal(t, signature, "cae58ce99e9bb30b1819e8dfdced3261c5a827e488553c70f42cb27ce025cd93")
	fmt.Println(signature)
}

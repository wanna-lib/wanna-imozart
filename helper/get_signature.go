package imozart_helper

import (
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"golang.org/x/crypto/pbkdf2"
)

func GetXSignature(salt string, pwd map[string]interface{}) (string, error) {
	b, err := json.Marshal(pwd)
	if err != nil {
		return "", err
	}

	saltByte := []byte(salt)
	encodedString := hex.EncodeToString(pbkdf2.Key(b, saltByte, 1000, 32, sha512.New))
	return encodedString, nil
}

package imozart

import (
	"gitlab.com/wanna-lib/wanna-imozart/agent"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"testing"
)

var agentCtx = Init(Configuration{
	AgentHost: "https://ag.imozart.com",
	Username:  "xxx",
	Password:  "xxx",
})

const token = ""

func TestAgentLogin(t *testing.T) {
	var responseLogin agent.ResponseLogin
	if err := agentCtx.Start().AgentCtx.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}
	imozart_helper.PrintStructJson(responseLogin)
}

func TestAgentProvider(t *testing.T) {
	var responseProvider agent.ResponseProvider
	if err := agentCtx.Start().AgentCtx.Provider(token, &responseProvider).Error(); err != nil {
		panic(err)
	}
	imozart_helper.PrintStructJson(responseProvider)
}

func TestAgentGetBetLimitPlayer(t *testing.T) {
	providerID := "60a60454f05d150011406a38"
	playerID := ""
	var responseGetBetLimitPlayer agent.ResponseGetBetLimitPlayer
	if err := agentCtx.Start().AgentCtx.GetBetLimitPlayer(token, providerID, playerID, &responseGetBetLimitPlayer).Error(); err != nil {
		panic(err)
	}
	imozart_helper.PrintStructJson(responseGetBetLimitPlayer)
}

func TestAgentBetLimitAndCommission(t *testing.T) {
	providerID := "6048e9cf309c52001b4861a4"
	playerID := "648dbe1e9c74c68e32db8627"
	prefix := "sa"
	var responseBetLimitAndCommission agent.ResponseBetLimitAndCommission
	if err := agentCtx.Start().AgentCtx.BetLimitAndCommission(agent.BetLimitAndCommissionRequest{
		ProviderId: providerID,
		Prefix:     prefix,
		PlayerId:   playerID,
		Commission: "0.00",
		BetLimit: []string{
			"1",
			"2",
			"1125899906842624",
			"2199023255552",
			"32",
		},
		IgnoreProvider: "false",
		Token:          token,
	}, &responseBetLimitAndCommission).Error(); err != nil {
		panic(err)
	}
	imozart_helper.PrintStructJson(responseBetLimitAndCommission)
}

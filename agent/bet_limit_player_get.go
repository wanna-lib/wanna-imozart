package agent

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type BetLimit struct {
	Id  string `json:"id"`
	Max string `json:"max"`
	Min string `json:"min"`
}

type ResponseGetBetLimitPlayer struct {
	Code                      int      `json:"code"`
	Success                   bool     `json:"success"`
	BetLimitList              []Json   `json:"betlimitList"`
	UserBetLimit              []string `json:"userBetlimit"`
	CommissionProviderInAgent int      `json:"commissionProviderInAgent"`
	CommissionUser            int      `json:"commissionUser"`
	IgnoreProvider            string   `json:"ignore_provider"`
	Message                   string   `json:"message"`
}

func (c context) GetBetLimitPlayer(token string, providerId string, playerId string, response *ResponseGetBetLimitPlayer) Context {
	url := c.conf.AgentHost + "/api/user/settings/getBetlimit?providerId=" + providerId + "&playerId=" + playerId
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return c.handleError(fmt.Errorf(err.Error()))
	}
	req.Header.Add("Authorization", "Bearer "+token)

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return c.handleError(fmt.Errorf(err.Error()))
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return c.handleError(fmt.Errorf(err.Error()))
	}

	if err := json.Unmarshal(body, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

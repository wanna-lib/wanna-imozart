package agent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"strconv"
)

type ResponseBetLimitAndCommission struct {
	Code        int         `json:"code"`
	Success     bool        `json:"success"`
	UserSetting interface{} `json:"userSetting"`
	Message     string      `json:"message"`
}

type BetLimitAndCommissionRequest struct {
	ProviderId     string   `json:"providerId"`
	Prefix         string   `json:"prefix"`
	PlayerId       string   `json:"playerId"`
	Commission     string   `json:"commission"`
	BetLimit       []string `json:"betlimit"`
	IgnoreProvider string   `json:"ignore_provider"`
	Token          string   `json:"token"`
}

func (c context) BetLimitAndCommission(req BetLimitAndCommissionRequest, response *ResponseBetLimitAndCommission) Context {

	headers := map[string]string{
		fiber.HeaderContentType:   "application/json",
		fiber.HeaderAuthorization: "Bearer " + req.Token,
	}

	bByte, _ := json.Marshal(req)
	var res requests.Response
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.AgentHost, `/api/user/settings/update/player/betlimitandcommission`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	var responseCall ResponseBetLimitAndCommission
	if err := json.Unmarshal(res.Result, &responseCall); err != nil {
		return c.handleError(err)
	}

	if responseCall.Code != 0 {
		return c.handleError(fmt.Errorf(strconv.Itoa(responseCall.Code) + " " + responseCall.Message))
	}

	*response = responseCall

	return &c
}

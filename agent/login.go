package agent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"strconv"
)

type ResponseLogin struct {
	Code    int    `json:"code"`
	Token   string `json:"token"`
	Fa      bool   `json:"fa"`
	Message string `json:"message"`
}

func (c context) Login(response *ResponseLogin) Context {

	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
	}

	bByte, _ := json.Marshal(Json{
		`username`: c.conf.Username,
		`password`: c.conf.Password,
	})

	var res requests.Response
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.AgentHost, `/api/auth/signin`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	var responseCall ResponseLogin
	if err := json.Unmarshal(res.Result, &responseCall); err != nil {
		return c.handleError(err)
	}

	if responseCall.Code != 0 {
		return c.handleError(fmt.Errorf(strconv.Itoa(responseCall.Code) + " " + responseCall.Message))
	}

	*response = responseCall

	return &c
}

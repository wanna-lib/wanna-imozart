package agent

type Configuration struct {
	AgentHost string
	Username  string
	Password  string
}

type (
	Json map[string]interface{}
)

type ErrorMessage struct {
	Code    int64  `json:"code"`
	Message string `json:"message"`
}

type (
	Context interface {
		Login(response *ResponseLogin) Context
		Provider(token string, response *ResponseProvider) Context
		GetBetLimitPlayer(token string, providerId string, playerId string, response *ResponseGetBetLimitPlayer) Context
		BetLimitAndCommission(req BetLimitAndCommissionRequest, response *ResponseBetLimitAndCommission) Context
		Error() error
	}
	context struct {
		conf Configuration
		err  error
	}
)

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func Init(conf Configuration) Context {
	return &context{
		conf: conf,
	}
}

func (c context) handleError(err error) Context {
	c.err = err
	return &c
}

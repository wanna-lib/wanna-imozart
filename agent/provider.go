package agent

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"reflect"
	"time"
)

type PvdCate struct {
	CategoryId  string      `json:"categoryId"`
	Prefix      string      `json:"prefix"`
	Description string      `json:"description"`
	Type        string      `json:"type"`
	Status      string      `json:"status"`
	Thumbnail   interface{} `json:"thumbnail"`
	CreatedAt   time.Time   `json:"createdAt"`
	UpdatedAt   time.Time   `json:"updatedAt"`
}

type ProviderCall struct {
	ProviderId   string      `json:"providerId"`
	Prefix       string      `json:"prefix"`
	Description  string      `json:"description"`
	Status       string      `json:"status"`
	Percent      string      `json:"percent"`
	Commission   string      `json:"commission"`
	Demo         bool        `json:"demo"`
	UrlAgentDev  string      `json:"urlAgentDev"`
	UrlAgentProd string      `json:"urlAgentProd"`
	UrlApiDev    string      `json:"urlApiDev"`
	UrlApiProd   string      `json:"urlApiProd"`
	Thumbnail    string      `json:"thumbnail"`
	News         bool        `json:"news"`
	Hits         bool        `json:"hits"`
	Specials     bool        `json:"specials"`
	IsMaintain   bool        `json:"is_maintain"`
	CreatedAt    time.Time   `json:"createdAt"`
	UpdatedAt    time.Time   `json:"updatedAt"`
	Id           string      `json:"_id"`
	Online       string      `json:"online"`
	PvdCate      []PvdCate   `json:"pvdcate"`
	BetLimit     interface{} `json:"betlimit"`
}

type Provider struct {
	ProviderId   string    `json:"providerId"`
	Prefix       string    `json:"prefix"`
	Description  string    `json:"description"`
	Status       string    `json:"status"`
	Percent      string    `json:"percent"`
	Commission   string    `json:"commission"`
	Demo         bool      `json:"demo"`
	UrlAgentDev  string    `json:"urlAgentDev"`
	UrlAgentProd string    `json:"urlAgentProd"`
	UrlApiDev    string    `json:"urlApiDev"`
	UrlApiProd   string    `json:"urlApiProd"`
	Thumbnail    string    `json:"thumbnail"`
	News         bool      `json:"news"`
	Hits         bool      `json:"hits"`
	Specials     bool      `json:"specials"`
	IsMaintain   bool      `json:"is_maintain"`
	CreatedAt    time.Time `json:"createdAt"`
	UpdatedAt    time.Time `json:"updatedAt"`
	Id           string    `json:"_id"`
	Online       string    `json:"online"`
	PvdCate      []PvdCate `json:"pvdcate"`
	BetLimit     []Json    `json:"betlimit"`
}

type ResponseProvider struct {
	Code    int        `json:"code"`
	Success bool       `json:"success"`
	Total   int        `json:"total"`
	List    []Provider `json:"list"`
}

type ResponseProviderCall struct {
	Code    int            `json:"code"`
	Success bool           `json:"success"`
	Total   int            `json:"total"`
	List    []ProviderCall `json:"list"`
}

func (c context) Provider(token string, response *ResponseProvider) Context {

	headers := map[string]string{
		fiber.HeaderContentType:   "application/json",
		fiber.HeaderAuthorization: "Bearer " + token,
	}

	var res requests.Response
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.AgentHost, `/api/manage/providers`),
		HEADERS: headers,
		TIMEOUT: 30,
	}
	if err := requests.Call().Get(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		return c.handleError(fmt.Errorf(string(res.Result)))
	}

	var resCall ResponseProviderCall
	if err := json.Unmarshal(res.Result, &resCall); err != nil {
		return c.handleError(err)
	}

	mapList := make([]Provider, 0)
	for _, provider := range resCall.List {
		if isArray(provider.BetLimit) {
			var pvd Provider
			imozart_helper.CopyStructToStruct(provider, &pvd)
			mapList = append(mapList, pvd)
		} else {
			resCall.Total--
		}
	}

	*response = ResponseProvider{
		Code:    resCall.Code,
		Success: resCall.Success,
		Total:   resCall.Total,
		List:    mapList,
	}

	return &c
}

func isArray(data interface{}) bool {
	val := reflect.ValueOf(data)
	return val.Kind() == reflect.Array || val.Kind() == reflect.Slice
}

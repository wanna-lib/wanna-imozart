package imozart

import (
	"gitlab.com/wanna-lib/wanna-imozart/agent"
	"gitlab.com/wanna-lib/wanna-imozart/game"
	"gitlab.com/wanna-lib/wanna-imozart/player"
)

type Configuration struct {
	SecretKey string
	Host      string
	Salt      string
	AgentHost string
	Username  string
	Password  string
}

type (
	Context interface {
		Start() context
		Error() error
	}
	context struct {
		PlayerCtx player.Context
		GameCtx   game.Context
		AgentCtx  agent.Context
		err       error
	}
)

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func Init(conf Configuration) Context {
	return &context{
		PlayerCtx: player.Init(player.Configuration{
			SecretKey: conf.SecretKey,
			Host:      conf.Host,
			Salt:      conf.Salt,
		}),
		GameCtx: game.Init(game.Configuration{
			SecretKey: conf.SecretKey,
			Host:      conf.Host,
			Salt:      conf.Salt,
		}),
		AgentCtx: agent.Init(agent.Configuration{
			AgentHost: conf.AgentHost,
			Username:  conf.Username,
			Password:  conf.Password,
		}),
	}
}

func (c context) Start() context {
	return c
}

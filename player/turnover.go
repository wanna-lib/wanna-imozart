package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type TurnOver struct {
	Game        string  `json:"game"`
	Amount      float64 `json:"amount"`
	Outstanding float64 `json:"outstanding"`
}

type ResponseTurnover struct {
	Code              int        `json:"code"`
	Success           bool       `json:"success"`
	PlayerApiId       string     `json:"playerApiId"`
	PlayerUsername    string     `json:"playerUsername"`
	PlayerApiUsername string     `json:"playerApiUsername"`
	TurnOver          []TurnOver `json:"turnOver"`
}

func (c context) Turnover(prefix string, username string, filter FilterDate, response *ResponseTurnover) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`startDate`:      filter.StartDate,
		`endDate`:        filter.EndDate,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	//delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	//turnover
	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/player/turnover`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

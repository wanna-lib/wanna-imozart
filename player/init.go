package player

import (
	"time"
)

type Configuration struct {
	SecretKey string
	Host      string
	Salt      string
}

type (
	Json map[string]interface{}
)

type GameLogList struct {
	Timestamp       time.Time `json:"timestamp"`
	Username        string    `json:"username"`
	BeforeCredit    string    `json:"beforeCredit"`
	AfterCredit     string    `json:"afterCredit"`
	TransactionType string    `json:"transactionType"`
	GameName        string    `json:"gameName"`
	GameType        string    `json:"gameType"`
	Status          string    `json:"status"`
	Amount          float64   `json:"amount"`
}

type ResponseGameLog struct {
	Code              int           `json:"code"`
	Success           bool          `json:"success"`
	PlayerApiId       string        `json:"playerApiId"`
	PlayerUsername    string        `json:"playerUsername"`
	PlayerApiUsername string        `json:"playerApiUsername"`
	List              []GameLogList `json:"list"`
}

type (
	Context interface {
		Create(prefix string, req RequestCreate, response *ResponseCreatePlayer) Context                //✅
		Balance(prefix string, username string, response *ResponseBalance) Context                      //✅
		Deposit(prefix string, username string, amount float64, response *ResponseDeposit) Context      //✅
		Withdraw(prefix string, username string, amount float64, response *ResponseWithdraw) Context    //✅
		History(prefix string, req RequestHistory, response *ResponseHistory) Context                   //✅
		Turnover(prefix string, username string, filter FilterDate, response *ResponseTurnover) Context //✅
		GameLog(prefix string, username string, filter FilterDate, response *ResponseGameLog) Context   //✅
		SessionDestroy(prefix string, username string, response *ResponseSessionDestroy) Context        //✅
		GetFreeSpin(prefix string, username string, response *ResponseGetFreeSpin) Context              //✅
		GetOutstanding(prefix string, username string, response *ResponseGetOutstanding) Context        //✅
		Error() error
	}
	context struct {
		conf Configuration
		err  error
	}
)

func (c context) handleError(err error) Context {
	c.err = err
	return &c
}

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func Init(conf Configuration) Context {
	return &context{
		conf: conf,
	}
}

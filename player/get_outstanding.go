package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type OutstandingData struct {
	Amount         float64 `json:"amount"`
	Gametype       string  `json:"gametype"`
	Gamename       string  `json:"gamename"`
	GameId         string  `json:"gameId"`
	Partner        string  `json:"partner"`
	Status         string  `json:"status"`
	ProviderPrefix string  `json:"providerPrefix"`
}

type ResponseGetOutstanding struct {
	Success        bool              `json:"success"`
	Code           int               `json:"code"`
	PlayerUsername string            `json:"playerUsername"`
	Data           []OutstandingData `json:"data"`
}

func (c context) GetOutstanding(prefix string, username string, response *ResponseGetOutstanding) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}
	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/player/getOutstanding`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

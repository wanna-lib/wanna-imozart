package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type ResponseDeposit struct {
	Success       bool    `json:"success"`
	Code          int     `json:"code"`
	Balance       float64 `json:"balance"`
	BeforeBalance float64 `json:"beforeBalance"`
	AfterBalance  float64 `json:"afterBalance"`
}

func (c context) Deposit(prefix string, username string, amount float64, response *ResponseDeposit) Context {

	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`amount`:         amount,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/player/deposit`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

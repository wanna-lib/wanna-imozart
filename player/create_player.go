package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type ResponseCreatePlayer struct {
	Success           bool   `json:"success"`
	Code              int    `json:"code"`
	PlayerApiId       string `json:"playerApiId"`
	PlayerApiUsername string `json:"playerApiUsername"`
	PlayerUsername    string `json:"playerUsername"`
}

type RequestCreate struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (c context) Create(prefix string, req RequestCreate, response *ResponseCreatePlayer) Context {

	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: req.Username,
		`playerPassword`: req.Password,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/player/create`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

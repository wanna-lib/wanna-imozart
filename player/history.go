package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"time"
)

type Pagination struct {
	Page    int `json:"page"`
	PerPage int `json:"perPage"`
}

type FilterDate struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
}

type HistoryList struct {
	TransactionId     string    `json:"transactionId"`
	PlayerApiId       string    `json:"playerApiId"`
	PlayerUsername    string    `json:"playerUsername"`
	PlayerApiUsername string    `json:"playerApiUsername"`
	Type              string    `json:"type"`
	Amount            float64   `json:"amount"`
	Status            string    `json:"status"`
	Timestamp         time.Time `json:"timestamp"`
	AfterBalance      float64   `json:"afterBalance"`
	Method            string    `json:"method"`
}

type ResponseHistory struct {
	Success bool          `json:"success"`
	Total   int           `json:"total"`
	Code    int           `json:"code"`
	List    []HistoryList `json:"list"`
}

type RequestHistory struct {
	Username   string     `json:"username"`
	Pagination Pagination `json:"pagination"`
	Filter     FilterDate `json:"filter"`
}

func (c context) History(prefix string, req RequestHistory, response *ResponseHistory) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: req.Username,
		`page`:           req.Pagination.Page,
		`perPage`:        req.Pagination.PerPage,
		`startDate`:      req.Filter.StartDate,
		`endDate`:        req.Filter.EndDate,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	//delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/player/history`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}

package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"time"
)

type User struct {
	Id                int       `json:"id"`
	PlayerId          string    `json:"playerId"`
	PlayerUsername    string    `json:"playerUsername"`
	PlayerApiUsername string    `json:"playerApiUsername"`
	AgentId           string    `json:"agentId"`
	AgentPrefix       string    `json:"agentPrefix"`
	AgentCode         string    `json:"agentCode"`
	AgentRef          string    `json:"agentRef"`
	Balance           float64   `json:"balance"`
	Outstanding       float64   `json:"outstanding"`
	Status            string    `json:"status"`
	CreatedAt         time.Time `json:"createdAt"`
	UpdatedAt         time.Time `json:"updatedAt"`
}

type ResponseSessionDestroy struct {
	Code    int  `json:"code"`
	Success bool `json:"success"`
	User    User `json:"user"`
}

func (c context) SessionDestroy(prefix string, username string, response *ResponseSessionDestroy) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/session/destroy`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

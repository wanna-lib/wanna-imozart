package player

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type FreeSpinData struct {
	Amount         string `json:"amount"`
	Gametype       string `json:"gametype"`
	Gamename       string `json:"gamename"`
	GameId         string `json:"gameId"`
	Partner        string `json:"partner"`
	Status         string `json:"status"`
	Providerprefix string `json:"providerprefix"`
}

type ResponseGetFreeSpin struct {
	Success        bool           `json:"success"`
	Code           int            `json:"code"`
	PlayerUsername string         `json:"playerUsername"`
	Data           []FreeSpinData `json:"data"`
}

func (c context) GetFreeSpin(prefix string, username string, response *ResponseGetFreeSpin) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}
	var res requests.Response
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/player/getFreespin`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

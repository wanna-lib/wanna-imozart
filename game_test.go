package imozart

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/wanna-lib/wanna-imozart/game"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
	"testing"
)

type all struct {
	Partner game.Provider `json:"partner"`
	Games   []game.Game   `json:"games"`
}

func TestProvider(t *testing.T) {
	start := ctx.Start()
	var response game.ResponseProvider
	if err := start.GameCtx.Provider(prefix, &response).Error(); err != nil {
		panic(err)
	}

	fmt.Println(len(response.List))

	imozart_helper.PrintStructJson(response.List)

	//partners := make([]string, 0)
	//data := make([]all, 0)
	//for _, provider := range response.List {
	//	var responseGame game.ResponseGames
	//	if err := start.GameCtx.Games(prefix,"testuser02", provider.Partner, &responseGame).Error(); err != nil {
	//		panic(err)
	//	}
	//	data = append(data, all{
	//		Partner: provider,
	//		Games:   responseGame.List,
	//	})
	//
	//	//fmt.Println(data)
	//	//partners = append(partners, provider.Image)
	//	//imozart_helper.PrintStructJson(provider.Image)
	//}
	//b, err := json.Marshal(data)
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}
	//err = ioutil.WriteFile("output.txt", b, 0644)
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println(string(b))

	//imozart_helper.PrintStructJson(partners)

	//c := 0
	//for _, provider := range response.List {
	//	username := "testuser02"
	//	var response game.ResponseGames
	//	if err := start.GameCtx.Games(prefix,username, provider.Partner, &response).Error(); err != nil {
	//		panic(err)
	//	}
	//	c += len(response.List)
	//}
	//fmt.Println(c)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	assert.Equal(t, response.Success, true)
}

func TestCategories(t *testing.T) {
	start := ctx.Start()

	req := game.RequestCategories{
		Username:   "unicoqnoasqlitb8",
		Categories: []string{"slot"},
		Type:       game.CategoryTypeNormal,
	}
	var response game.ResponseCategories
	if err := start.GameCtx.Categories(prefix, req, &response).Error(); err != nil {
		panic(err)
	}

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	imozart_helper.PrintStructJson(response)
	assert.Equal(t, response.Success, true)
}

func TestFavorite(t *testing.T) {
	//start := ctx.Start()
	//
	//req := game.RequestFavorite{
	//	Username: "testuser02",
	//	Type:     game.TypeGameFollow,
	//	Partner:  "sa",
	//	GameID:   "dtx",
	//}
	//
	//var response game.ResponseFavorite
	//if err := start.GameCtx.Favorite(prefix, req, &response).Error(); err != nil {
	//	panic(err)
	//}
	//
	//imozart_helper.PrintStructJson(response)
	//
	//if !response.Success {
	//	panic(imozart_helper.ErrorCodeString[response.Code])
	//}
	//assert.Equal(t, response.Success, true)
}

type gameData struct {
	Name   string `json:"name"`
	Image  string `json:"image"`
	Status string `json:"status"`
}

func TestGames(t *testing.T) {
	start := ctx.Start()
	//username := "kenssotevw17v7ew"
	username := "unicoqnoasqlitb8"
	//username := "kenssotevw17v7ew"
	partner := "pgs"

	var response game.ResponseGames
	if err := start.GameCtx.Games(prefix, username, partner, []string{"slot"}, &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response.List)

	//data := make([]forExcel, 0)
	//for _, g := range response.List {
	//	data = append(data, forExcel{
	//		Partner: g.Description,
	//		Image:   g.Image,
	//		Status:  g.Status,
	//	})
	//	//imozart_helper.PrintStructJson(g.Description)
	//	//imozart_helper.PrintStructJson()
	//}
	//fmt.Println(data)
	//fmt.Println(len(response.List))
	imozart_helper.PrintStructJson(response.List)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	assert.Equal(t, response.Success, true)
}

func TestNewGames(t *testing.T) {
	start := ctx.Start()
	partner := "pgn"

	var response game.ResponseGamesV3
	if err := start.GameCtx.NewGames(game.NewGamesRequest{
		Prefix:                prefix,
		ProviderPrefix:        partner,
		Category:              "slot",
		SpecialCategorySelect: "",
		Page:                  1,
		PerPage:               20,
	}, &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response)
	imozart_helper.PrintStructJson(response.GamesList)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	assert.Equal(t, response.Success, true)
}

func TestLauncher(t *testing.T) {
	start := ctx.Start()

	req := game.RequestLauncher{
		Username: "bluelhqmoci4llc9",
		Partner:  "pp",
		GameID:   "vswaysultrcoin",
		Setting: game.Setting{
			Lang: "th-TH",
			Eurl: "google.com",
		},
	}

	var response game.ResponseLauncher
	if err := start.GameCtx.Launcher(prefix, req, &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response)
	fmt.Println(response.Launcher.Desktop)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	assert.Equal(t, response.Success, true)
}

func TestLauncherDemo(t *testing.T) {
	start := ctx.Start()

	var response game.ResponseLauncherDemo
	if err := start.GameCtx.LauncherDemo("endo", "endorphina2_BookOfLady@ENDORPHINA", "https://google.com", &response).Error(); err != nil {
		panic(err)
	}

	imozart_helper.PrintStructJson(response)

	if !response.Success {
		panic(imozart_helper.ErrorCodeString[response.Code])
	}
	assert.Equal(t, response.Success, true)
}

//func TestLobby(t *testing.T) {
//	start := ctx.Start()
//	username := "testuser02"
//	partner := "ka"
//
//	var response game.ResponseLobby
//	if err := start.GameCtx.Lobby(prefix,username, partner, &response).Error(); err != nil {
//		panic(err)
//	}
//
//	imozart_helper.PrintStructJson(response)
//
//	if !response.Success {
//		panic(imozart_helper.ErrorCodeString[response.Code])
//	}
//	assert.Equal(t, response.Success, true)
//}

module gitlab.com/wanna-lib/wanna-imozart

go 1.15

require (
	github.com/gofiber/fiber v1.14.6
	github.com/stretchr/testify v1.8.1
	gitlab.com/trendgolibrary/trend-call v1.0.3
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)

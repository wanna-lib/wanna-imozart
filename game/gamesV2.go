package game

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type GamesList struct {
	GameId      string   `json:"gameId"`
	Partner     string   `json:"partner"`
	Description string   `json:"description"`
	Status      string   `json:"status"`
	Image       string   `json:"image"`
	Categories  []string `json:"categories"`
	Online      string   `json:"online"`
	Demo        bool     `json:"demo"`
	Hits        bool     `json:"hits"`
	News        bool     `json:"news"`
	Specials    bool     `json:"specials"`
	Ranking     int      `json:"ranking"`
}

type ResponseGamesV3 struct {
	Success   bool        `json:"success"`
	Code      int         `json:"code"`
	Message   string      `json:"message"`
	Timestamp int64       `json:"timestamp"`
	GamesList []GamesList `json:"list"`
	Total     int         `json:"total"`
}

type OrderRequest struct {
	CreatedAt string `json:"createdAt"`
}

type NewGamesRequest struct {
	Prefix                string       `json:"prefix"`
	ProviderPrefix        string       `json:"providerPrefix"`
	Category              string       `json:"category"`
	SpecialCategorySelect string       `json:"specialCategorySelect"`
	Page                  int          `json:"page"`
	PerPage               int          `json:"perPage"`
	Order                 OrderRequest `json:"order"`
}

func (c context) NewGames(req NewGamesRequest, response *ResponseGamesV3) Context {
	if req.Order.CreatedAt == "" {
		req.Order.CreatedAt = "DESC"
	}
	bodySignatureHeader := Json{
		`prefix`:                req.Prefix,
		`providerPrefix`:        req.ProviderPrefix,
		`category`:              req.Category,
		`specialCategorySelect`: req.SpecialCategorySelect,
		`page`:                  req.Page,
		`perPage`:               req.PerPage,
		`order`:                 req.Order,
		`secretKey`:             c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	//delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}
	//fmt.Println(imozart_helper.JoinStr(c.conf.Host, `/new/get/games`))
	//imozart_helper.PrintStructJson(bodySignatureHeader)
	//imozart_helper.PrintStructJson(headers)
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/new/get/games`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	//fmt.Println(string(res.Result))

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

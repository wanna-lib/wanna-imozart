package game

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type Game struct {
	Id             string   `json:"_id"`
	FavoriteNumber int      `json:"favoriteNumber"`
	PlayNumber     int      `json:"playNumber"`
	Status         string   `json:"status"`
	Demo           bool     `json:"demo"`
	GameId         string   `json:"gameId"`
	Partner        string   `json:"partner"`
	Description    string   `json:"description"`
	Image          string   `json:"image"`
	Categories     []string `json:"categories"`
	Online         string   `json:"online"`
}

type ResponseGames struct {
	Code    int    `json:"code"`
	Success bool   `json:"success"`
	List    []Game `json:"list"`
}

func (c context) Games(prefix string, username string, partner string, categories []string, response *ResponseGames) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`partner`:        partner,
		`categories`:     categories,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}
	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/get/games`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

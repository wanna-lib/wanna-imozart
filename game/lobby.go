package game

import (
	"github.com/gofiber/fiber"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type ResponseLobby struct {
	Code     int      `json:"code"`
	Success  bool     `json:"success"`
	Launcher Launcher `json:"launcher"`
}

func (c context) Lobby(prefix string, username string, partner string, response *ResponseLobby) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: username,
		`partner`:        partner,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	imozart_helper.PrintStructJson(bodySignatureHeader)
	imozart_helper.PrintStructJson(headers)
	return &c
}

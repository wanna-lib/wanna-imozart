package game

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type Launcher struct {
	Desktop string `json:"desktop"`
	Mobile  string `json:"mobile"`
}

type ResponseLauncher struct {
	Code     int      `json:"code"`
	Success  bool     `json:"success"`
	Launcher Launcher `json:"launcher"`
}

type Setting struct {
	Lang string `json:"lang"`
	Eurl string `json:"eurl"`
}

type RequestLauncher struct {
	Username string  `json:"username"`
	Partner  string  `json:"partner"`
	GameID   string  `json:"gameID"`
	Setting  Setting `json:"setting"`
}

func (c context) Launcher(prefix string, req RequestLauncher, response *ResponseLauncher) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: req.Username,
		`partner`:        req.Partner,
		`game`:           req.GameID,
		`settings`: Json{
			`lang`: req.Setting.Lang,
			`eurl`: req.Setting.Eurl,
		},
		`secretKey`: c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	//delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/launcher`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

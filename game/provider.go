package game

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type Provider struct {
	Partner       string   `json:"partner"`
	Image         string   `json:"image"`
	Categories    []string `json:"categories"`
	Description   string   `json:"description"`
	Status        string   `json:"status"`
	IsMaintenance bool     `json:"isMaintenance"`
}
type ResponseProvider struct {
	Code    int        `json:"code"`
	Success bool       `json:"success"`
	List    []Provider `json:"list"`
}

func (c context) Provider(prefix string, response *ResponseProvider) Context {
	bodySignatureHeader := Json{
		`prefix`:    prefix,
		`secretKey`: c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/provider`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}

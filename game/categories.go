package game

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type RequestCategories struct {
	Username   string   `json:"username"`
	Categories []string `json:"categories"`
	Type       string   `json:"type"`
}

type Category struct {
	Description string `json:"description"`
	Category    string `json:"category"`
	Image       string `json:"image"`
	Status      string `json:"status"`
}

type ResponseCategories struct {
	Code    int        `json:"code"`
	Success bool       `json:"success"`
	List    []Category `json:"list"`
}

var (
	CategoryTypeSpecial = "special"
	CategoryTypeNormal  = "normal"
)

func (c context) Categories(prefix string, req RequestCategories, response *ResponseCategories) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`categories`:     req.Categories,
		`type`:           req.Type,
		`playerUsername`: req.Username,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	//delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/categories`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

package game

import (
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
)

type ResponseLauncherDemo struct {
	Code     int      `json:"code"`
	Success  bool     `json:"success"`
	Launcher Launcher `json:"launcher"`
}

func (c context) LauncherDemo(partner string, gameID string, urlRedirect string, response *ResponseLauncherDemo) Context {
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
	}
	var resRequest requests.Response
	if err := requests.Call().Get(requests.Params{
		URL: c.conf.Host + "/launcher/demo",
		QUERY: map[string]string{
			`partner`:        partner,
			`gameId`:         gameID,
			`settings[eurl]`: urlRedirect,
		},
		HEADERS: headers,
		BODY:    nil,
		TIMEOUT: 30,
	}, &resRequest).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(resRequest.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}

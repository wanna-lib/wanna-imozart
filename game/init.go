package game

type Configuration struct {
	SecretKey string
	Host      string
	Salt      string
}

var (
	TypeGameFollow   = "follow"
	TypeGameUnfollow = "unfollow"
)

type (
	Json map[string]interface{}
)

type (
	Context interface {
		Favorite(prefix string, req RequestFavorite, response *ResponseFavorite) Context
		Launcher(prefix string, req RequestLauncher, response *ResponseLauncher) Context
		Lobby(prefix string, username string, partner string, response *ResponseLobby) Context
		Provider(prefix string, response *ResponseProvider) Context
		Games(prefix string, username string, partner string, categories []string, response *ResponseGames) Context
		NewGames(req NewGamesRequest, response *ResponseGamesV3) Context
		Categories(prefix string, req RequestCategories, response *ResponseCategories) Context
		LauncherDemo(partner string, gameID string, urlRedirect string, response *ResponseLauncherDemo) Context
		Error() error
	}
	context struct {
		conf Configuration
		err  error
	}
)

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func Init(conf Configuration) Context {
	return &context{
		conf: conf,
	}
}

func (c context) handleError(err error) Context {
	c.err = err
	return &c
}

package game

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	imozart_helper "gitlab.com/wanna-lib/wanna-imozart/helper"
)

type FavoriteUser struct {
	Favorites []string `json:"favorites"`
	Id        string   `json:"_id"`
	Username  string   `json:"username"`
}

type ResponseFavorite struct {
	Code    int          `json:"code"`
	Success bool         `json:"success"`
	User    FavoriteUser `json:"user"`
}

type RequestFavorite struct {
	Username string `json:"username"`
	Type     string `json:"type"`
	Partner  string `json:"partner"`
	GameID   string `json:"gameID"`
}

func (c context) Favorite(prefix string, req RequestFavorite, response *ResponseFavorite) Context {
	bodySignatureHeader := Json{
		`prefix`:         prefix,
		`playerUsername`: req.Username,
		`partner`:        req.Partner,
		`game`:           req.GameID,
		`type`:           req.Type,
		`secretKey`:      c.conf.SecretKey,
	}

	signature, err := imozart_helper.GetXSignature(c.conf.Salt, bodySignatureHeader)
	if err != nil {
		return c.handleError(err)
	}

	//delete(bodySignatureHeader, "secretKey")

	//var res requests.Response
	headers := map[string]string{
		fiber.HeaderContentType: "application/json",
		"x-signature":           signature,
	}

	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     imozart_helper.JoinStr(c.conf.Host, `/games/favorite`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}

	return &c
}
